/*******************************************************************************
 * Copyright 2011 See AUTHORS file.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package com.badlogicgames.superjumper;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Net;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.net.Socket;
import com.badlogic.gdx.net.SocketHints;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogicgames.superjumper.World.WorldListener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

public class GameScreen extends ScreenAdapter {
	static final int GAME_READY = 0;
	static final int GAME_RUNNING = 1;
	static final int GAME_PAUSED = 2;
	static final int GAME_LEVEL_END = 3;
	static final int GAME_OVER = 4;
	static final int GAME_WAITINGPLAYER = 5;
	static final int GAME_CHALLENGEDEAD = 6;
	static final int GAME_CHALLENGEWIN = 7;
	static final int GAME_CONNECTIONERROR = 8;

	SuperJumper game;

	int state;
	OrthographicCamera guiCam;
	Vector3 touchPoint;
	World world;
	WorldListener worldListener;
	WorldRenderer renderer;
	Rectangle pauseBounds;
	Rectangle resumeBounds;
	Rectangle quitBounds;
	int lastScore;
	String scoreString;
	String lifeString;
    String heightString;
	String challengeString;
	boolean challenge;

	Socket socket;
	int playerId;
	int sendtimer;

	GlyphLayout glyphLayout = new GlyphLayout();

	public GameScreen (SuperJumper game, boolean challenge) {
		this.game = game;
		this.challenge = challenge;
		state = GAME_READY;

		if(challenge){
			Random rand = new Random();
			playerId = rand.nextInt(100000);
			challengeString = "CHALLENGER:";
			state = GAME_WAITINGPLAYER;
			initNet();
			sendtimer=0;
		}


		guiCam = new OrthographicCamera(320, 480);
		guiCam.position.set(320 / 2, 480 / 2, 0);
		touchPoint = new Vector3();
		worldListener = new WorldListener() {
			@Override
			public void jump () {
				Assets.playSound(Assets.jumpSound);
				Assets.jumpEffect.setPosition(world.bob.position.x, world.bob.position.y);
				Assets.jumpEffect.start();
			}

			@Override
			public void highJump () {
				Assets.playSound(Assets.highJumpSound);
			}

			@Override
			public void hit () {
				Assets.playSound(Assets.hitSound);
				Assets.squirrelDeath.setPosition(world.bob.position.x, world.bob.position.y);
				Assets.squirrelDeath.start();
			}

			@Override
			public void coin () {
				Assets.playSound(Assets.coinSound);
			}

		};
		world = new World(worldListener);
		renderer = new WorldRenderer(game.batcher, world);
		pauseBounds = new Rectangle(320 - 64, 480 - 64, 64, 64);
		resumeBounds = new Rectangle(160 - 96, 240, 192, 36);
		quitBounds = new Rectangle(160 - 96, 240 - 36, 192, 36);
		lastScore = 0;
		scoreString = "SCORE: 0";
		lifeString = "LIFES: 2";
        heightString = "HEIGHT: 0m";
	}

	public void update (float deltaTime) {
		if (deltaTime > 0.1f) deltaTime = 0.1f;

		switch (state) {
		case GAME_READY:
			updateReady();
			break;
		case GAME_RUNNING:
			updateRunning(deltaTime);
			break;
		case GAME_PAUSED:
			updatePaused();
			break;
		case GAME_LEVEL_END:
			updateLevelEnd();
			break;
		case GAME_OVER:
			updateGameOver();
			break;
		case GAME_CHALLENGEWIN:
			updateChallengeWin();
			break;
		case GAME_CONNECTIONERROR:
			updateConnectionError();
			break;
		}
	}

	private void updateConnectionError() {
		if (Gdx.input.justTouched()) {
			game.setScreen(new MainMenuScreen(game));
		}
	}
	private void updateChallengeWin() {
        if (socket.isConnected()) socket.dispose();
        if (Gdx.input.justTouched()) {
            game.setScreen(new MainMenuScreen(game));
        }
    }

    private void updateReady () {
		if (Gdx.input.justTouched()) {
			state = GAME_RUNNING;
		}
	}

	private void updateRunning (float deltaTime) {

		if(challenge){
			if(sendtimer<=0) {
				sendNet("JUMPER:" + String.valueOf(world.bob.position.y) + ":ALIVE");
				sendtimer = 50;
			}
			if (world.state == World.WORLD_STATE_NEXT_LEVEL) sendNet("FINISH");
			sendtimer--;
		}
		else{
			if (Gdx.input.justTouched()) {
				guiCam.unproject(touchPoint.set(Gdx.input.getX(), Gdx.input.getY(), 0));

				if (pauseBounds.contains(touchPoint.x, touchPoint.y)) {
					Assets.playSound(Assets.clickSound);
					state = GAME_PAUSED;
					return;
				}
			}
			if (world.score != lastScore) {
				lastScore = world.score;
				scoreString = "SCORE: " + lastScore;
			}
			if (world.state == World.WORLD_STATE_NEXT_LEVEL) {
				game.setScreen(new WinScreen(game));
			}
		}

		ApplicationType appType = Gdx.app.getType();
		// should work also with Gdx.input.isPeripheralAvailable(Peripheral.Accelerometer)
		if (appType == ApplicationType.Android || appType == ApplicationType.iOS) {
			world.update(deltaTime, Gdx.input.getAccelerometerX());
		} else {
			float accel = 0;
			if (Gdx.input.isKeyPressed(Keys.DPAD_LEFT)) accel = 5f;
			if (Gdx.input.isKeyPressed(Keys.DPAD_RIGHT)) accel = -5f;
			world.update(deltaTime, accel);
		}
		lifeString = "LIFES: " + world.lifes;
        heightString = "HEIGHT: "+ (int)world.bob.position.y+"m";


		if (world.state == World.WORLD_STATE_GAME_OVER) {
			if(!challenge)
			{
				state = GAME_OVER;
				if (lastScore >= Settings.highscores[4])
					scoreString = "NEW HIGHSCORE: " + lastScore;
				else
					scoreString = "SCORE: " + lastScore;
				Settings.addScore(lastScore);
				Settings.save();
			}
			else {
				state = GAME_CHALLENGEDEAD;
				sendNet("JUMPER:"+String.valueOf(world.bob.position.y)+":DEAD");
			}
		}
	}

	private void updatePaused () {
		if (Gdx.input.justTouched()) {
			guiCam.unproject(touchPoint.set(Gdx.input.getX(), Gdx.input.getY(), 0));

			if (resumeBounds.contains(touchPoint.x, touchPoint.y)) {
				Assets.playSound(Assets.clickSound);
				state = GAME_RUNNING;
				return;
			}

			if (quitBounds.contains(touchPoint.x, touchPoint.y)) {
				Assets.playSound(Assets.clickSound);
				game.setScreen(new MainMenuScreen(game));
				return;
			}
		}
	}

	private void updateLevelEnd () {
		if (Gdx.input.justTouched()) {
			world = new World(worldListener);
			renderer = new WorldRenderer(game.batcher, world);
			world.score = lastScore;
			state = GAME_READY;
		}
	}

	private void updateGameOver () {
        if(challenge) {
			if(socket.isConnected()) socket.dispose();
		}
		if (Gdx.input.justTouched()) {
			game.setScreen(new MainMenuScreen(game));
		}
	}

	public void draw () {
		GL20 gl = Gdx.gl;
		gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		renderer.render();

		guiCam.update();
		game.batcher.setProjectionMatrix(guiCam.combined);
		game.batcher.enableBlending();
		game.batcher.begin();
		switch (state) {
			case GAME_READY:
				presentReady();
				break;
			case GAME_RUNNING:
				presentRunning();
				break;
			case GAME_PAUSED:
				presentPaused();
				break;
			case GAME_LEVEL_END:
				presentLevelEnd();
				break;
			case GAME_OVER:
				presentGameOver();
				break;
			case GAME_WAITINGPLAYER:
				presentWaitingPlayer();
				break;
			case GAME_CHALLENGEDEAD:
				presentChallengeDead();
				break;
			case GAME_CHALLENGEWIN:
				presentChallengeWin();
				break;
			case GAME_CONNECTIONERROR:
				presentConnectionError();
				break;
		}
		game.batcher.end();
	}

	private void presentConnectionError() {
		Assets.font.draw(game.batcher, "CONNECTION WITH", 16, 480 - 40);
		Assets.font.draw(game.batcher, "CHALLENGER LOST", 16, 480 - 60);
	}

	private void presentChallengeWin() {
		Assets.font.draw(game.batcher, "YOU WIN", 16, 480 - 40);
		Assets.font.draw(game.batcher, heightString, 16, 480 - 60);
		Assets.font.draw(game.batcher, challengeString, 16, 480 - 80);
	}

	private void presentChallengeDead() {
		Assets.font.draw(game.batcher, "YOU DIED", 24, 480 - 60);
		Assets.font.draw(game.batcher, "WAITING FOR OTHER", 5, 480 - 80);
		Assets.font.draw(game.batcher, "CHALLENGER TO FINISH", 5, 480 - 100);
		Assets.font.draw(game.batcher, heightString, 16, 480 - 120);
	}

	private void presentWaitingPlayer() {
		Assets.font.draw(game.batcher, "WAITING FOR", 16, 480 - 60);
		Assets.font.draw(game.batcher, "CHALLENGER", 16, 480 - 80);
	}

	private void presentReady () {
		game.batcher.draw(Assets.ready, 160 - 192 / 2, 240 - 32 / 2, 192, 32);
	}

	private void presentRunning () {
		if(challenge) Assets.font.draw(game.batcher, challengeString, 16, 480 - 20);
		else {
			game.batcher.draw(Assets.pause, 320 - 64, 480 - 64, 64, 64);
			Assets.font.draw(game.batcher, scoreString, 16, 480 - 20);
		}
		Assets.font.draw(game.batcher, heightString, 16, 480 - 40);
		Assets.font.draw(game.batcher, lifeString, 16, 480 - 60);
	}

	private void presentPaused () {
		game.batcher.draw(Assets.pauseMenu, 160 - 192 / 2, 240 - 96 / 2, 192, 96);
		Assets.font.draw(game.batcher, scoreString, 16, 480 - 20);
	}

	private void presentLevelEnd () {
		glyphLayout.setText(Assets.font, "the princess is ...");
		Assets.font.draw(game.batcher, glyphLayout, 160 - glyphLayout.width / 2, 480 - 40);
		glyphLayout.setText(Assets.font, "in another castle!");
		Assets.font.draw(game.batcher, glyphLayout, 160 - glyphLayout.width / 2, 40);
	}

	private void presentGameOver () {
		if(!challenge){
			game.batcher.draw(Assets.gameOver, 160 - 160 / 2, 240 - 96 / 2, 160, 96);
			glyphLayout.setText(Assets.font, scoreString);
			Assets.font.draw(game.batcher, scoreString, 160 - glyphLayout.width / 2, 480 - 20);
		}
		else{
			Assets.font.draw(game.batcher, "YOU LOST", 16, 480 - 40);
			Assets.font.draw(game.batcher, heightString, 16, 480 - 60);
			Assets.font.draw(game.batcher, challengeString, 16, 480 - 80);
		}
	}

	@Override
	public void render (float delta) {
		update(delta);
		draw();
	}

	@Override
	public void pause () {
		if (state == GAME_RUNNING && !challenge) state = GAME_PAUSED;
	}


	// CHALLENGE STUFF
	private boolean initNet() {
		try {
			if (socket != null && socket.isConnected()) return true;
			socket = Gdx.net.newClientSocket(Net.Protocol.TCP, "127.0.0.1", 43234 ,new SocketHints());
			sendNet("JOIN:" + playerId+":JUMPER");
			new Thread(new Runnable(){
				@Override
				public void run() {
					while(true){
						if (socket!=null && socket.isConnected()) {
							BufferedReader buffer = new BufferedReader(new
									InputStreamReader(socket.getInputStream()));
							try {
								String string = buffer.readLine();
								Gdx.app.log("net-app", "recieved-> " + string);
								String[] msgs = string.split("-");
								for (String msg: msgs) {
									Gdx.app.log("net-app" , "processing-> " + msg);
									processGameData(msg);
								}

							} catch (IOException e) {
								e.printStackTrace();
								state = GAME_CONNECTIONERROR;
							}
						}
					}
				}
			}).start();
			return true;
		} catch (GdxRuntimeException e) {
			e.printStackTrace();
			state = GAME_CONNECTIONERROR;
			return false;
		}
	}

	private void processGameData(String msg) {
		String[] data = msg.split(":");
		if(data[0].equals("STATE")){
			state = Integer.parseInt(data[1]);
		}
		if(data[0].equals("HEIGHT")){
			float height = Float.parseFloat(data[1]);
			challengeString = "CHALLENGER: "+ (int)height+"m";
		}
	}

	private void sendNet(String msg) {
		if (socket == null) {
			Gdx.app.log("net-app", "Not socket, not sending");
			return;
		}
		try {
			Gdx.app.log("net-app", "sending-> " + msg);
			socket.getOutputStream().write(msg.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}



}