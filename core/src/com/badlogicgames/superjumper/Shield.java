package com.badlogicgames.superjumper;

/**
 * Created by Axel on 16/04/2017.
 */

public class Shield extends GameObject {
    public static final float SHIELD_WIDTH = 0.5f;
    public static final float SHIELD_HEIGHT = 0.8f;
    float stateTime;

    public Shield(float x, float y) {
        super(x, y, SHIELD_WIDTH, SHIELD_HEIGHT);
    }

    public void update (float deltaTime) {
        stateTime += deltaTime;
    }
}
