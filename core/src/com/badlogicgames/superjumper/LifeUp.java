package com.badlogicgames.superjumper;

/**
 * Created by Axel on 16/04/2017.
 */

public class LifeUp extends GameObject{
    public static final float LIFEUP_WIDTH = 0.5f;
    public static final float LIFEUP_HEIGHT = 0.8f;
    public static final int LIFEUP_LIFE = 1;

    float stateTime;

    public LifeUp(float x, float y) {
        super(x, y, LIFEUP_WIDTH, LIFEUP_HEIGHT);
        stateTime = 0;
    }

    public void update (float deltaTime) {
        stateTime += deltaTime;
    }
}
